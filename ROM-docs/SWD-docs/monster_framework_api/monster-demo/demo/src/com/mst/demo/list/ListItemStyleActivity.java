package com.mst.demo.list;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.CompoundButton;

import com.mst.demo.R;
public class ListItemStyleActivity extends Activity {

	private ListView mList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_item_style_activity);
		mList = (ListView)findViewById(android.R.id.list);
		mList.setAdapter(new ListTestAdapter());
	}
	
	
	
	
	
	class ListTestAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 12;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return "Item";
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextView title = null;
			TextView summary = null;
			ImageView icon = null;
			LayoutInflater inflater = LayoutInflater.from(ListItemStyleActivity.this);
			int layoutRes  = 0/*= com.mst.R.layout.list_item_1_line*/;
			switch (position) {
			case 0:
				layoutRes = com.mst.R.layout.list_item_1_line_single_choice; 
				break;
			case 1:
				layoutRes = com.mst.R.layout.list_item_1_line_multiple_choice;
				break;
			case 2:
				layoutRes = com.mst.R.layout.list_item_1_line_with_icon;
				break;
			case 3:
				layoutRes = com.mst.R.layout.list_item_1_line_with_icon_single_choice;
				break;
			case 4:
				layoutRes = com.mst.R.layout.list_item_1_line_with_icon_multiple_choice;
				break;
			case 5:
				layoutRes = com.mst.R.layout.list_item_2_line;
				break;
			case 6:
				layoutRes = com.mst.R.layout.list_item_2_line_single_choice;
				break;
			case 7:
				layoutRes = com.mst.R.layout.list_item_2_line_multiple_choice;
				break;
				
			case 8:
				layoutRes = com.mst.R.layout.list_item_2_line_multiple_choice_with_icon;
				break;
			case 9:
				Log.d("pos", "position:"+position);
				layoutRes = com.mst.R.layout.list_item_2_line_with_icon;
				break;
			case 10:
				layoutRes = com.mst.R.layout.list_item_2_line_single_choice_with_icon;
				break;
			case 11:
				layoutRes = com.mst.R.layout.list_item_1_line;
				break;
			}
			if( layoutRes != 0){
				convertView = inflater.inflate(layoutRes, null);
				title = (TextView) convertView.findViewById(android.R.id.text1);
				summary = (TextView)convertView.findViewById(android.R.id.text2);
				icon = (ImageView)convertView.findViewById(android.R.id.icon);
				View view = convertView.findViewById(android.R.id.button1);
				if(title != null){
					title.setText("Item Title");
				}
				if(summary != null){
					summary.setText("Item Summary");
				}
				if(icon != null){
					icon.setImageResource(com.mst.demo.R.drawable.icon);
				}
				if(view != null){
					if(view instanceof CompoundButton){
						final CompoundButton checkbox = (CompoundButton) view;
						convertView.setOnClickListener(new View.OnClickListener(){
							@Override
							public void onClick(View v){
								checkbox.setChecked(!checkbox.isChecked());
							}
						});
					}
				}
			}
			return convertView;
		}
		
	}
	
	
	
	
}
