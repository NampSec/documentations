package com.mst.demo.material;

import mst.widget.toolbar.Toolbar;
import mst.widget.toolbar.Toolbar.OnMenuItemClickListener;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.mst.demo.R;
import com.mst.demo.util.FragmentUtils;
public class SliderLayoutFragment extends Fragment implements OnClickListener{
	private static final String 	TAG = "SliderLayoutFragment";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.md_slider_layout, container,false);
		View goToListView = view.findViewById(R.id.slide_in_list_btn);
		goToListView.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		FragmentUtils u = new FragmentUtils(getActivity());
		String listFragment = ListSliderLayoutFragment.class.getName();
		u.switchToFragment(listFragment, null, false, true, 0, "Slider In ListView", true);
	}

	

}
