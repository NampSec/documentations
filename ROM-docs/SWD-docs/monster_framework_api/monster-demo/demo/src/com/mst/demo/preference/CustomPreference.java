package com.mst.demo.preference;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import mst.preference.Preference;

import com.mst.demo.R;
public class CustomPreference extends Preference{

	
	private TextView mTitleView;
	
	private int mClickTime = 0;
	
	public CustomPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		setLayoutResource(R.layout.custom_pref_layout);
	}

	
	@Override
	protected void onBindView(View view) {
		super.onBindView(view);
			mTitleView = (TextView)view.findViewById(android.R.id.text1);
			if(mTitleView != null){
				mTitleView.setText("Custom Preference From Code:"+getClickedTime());
			}
	}


	public void updateClickedTime(int clickTime) {
		// TODO Auto-generated method stub
		mClickTime = clickTime;
		notifyChanged();
	}
	
	public int getClickedTime(){
		return mClickTime;
	}
	

}
