package com.mst.demo.indexbar;

import com.mst.demo.indexbar.HanziToPinyin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caizhongting on 16-8-9.
 */
public class Been {
    public static ArrayList<Contact> sData;
    public static String str_other = "其他";
    public static String[] peoples = {
            "123","456","789",
            "阿扁","阿彪","阿斌","阿斗","阿豆",
            "毕加索","毕业","部长",
            "陈彪","陈斌","陈晨","陈浩","陈浩然","陈林","陈依依","陈弋",
            "碉堡","丁丁",
            "贺斌","贺龙","贺先","黄小龙","何智",
            "吉安","纪晓岚",
            "刘备","刘邦","刘老板","李明","刘娜","李硕","李世石",
            "彭成","彭飞","彭强",
            "秦奎","秦始皇","秦志龙",
            "阮经天","阮元元",
            "宋飞",
            "易宏","易建","易中天",
            "张飞","张军","张可","张梦雪","张晓","张笑","张肖","张亦辉","张忠国"
    };

    public static ArrayList<Contact> getContacts(){
        if(sData == null) {
            sData = new ArrayList<>();
            HanziToPinyin engine = HanziToPinyin.getInstance();
            String last = null;
            for (String people : peoples) {
                Contact c = new Contact();
                c.name = people;
                c.pinyin = engine.transliterate(people);
                for (int i = 0; i < people.length(); i++) {
                    String fistLetter = engine.transliterate(people.substring(i,i+1)).substring(0,1);
                    if(i == 0){
                        if(!fistLetter.equals(last)){
                            Contact g = new Contact();
                            if(isLetter(fistLetter)) {
                                g.name = fistLetter;
                                g.type = 1;
                            }else{
                                g.name = str_other;
                                g.type = 1;
                                fistLetter = str_other;
                            }
                            if(!fistLetter.equals(last)) {
                                sData.add(g);
                            }
                        }
                        last = fistLetter;
                    }
                    c.firstLetter.add(fistLetter);
                }
                sData.add(c);
            }
        }
        return sData;
    }

    public static boolean isLetter(String cha){
        if(cha == null || cha.length() <= 0) return false;
        char first = cha.toUpperCase().charAt(0);
        if(first >= 'A' && first <= 'Z'){
            return true;
        }
        return false;
    }
}
