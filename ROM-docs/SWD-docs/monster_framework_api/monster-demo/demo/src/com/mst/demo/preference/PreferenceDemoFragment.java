package com.mst.demo.preference;

import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import mst.preference.CheckBoxPreference;
import mst.preference.EditTextPreference;
import mst.preference.ListPreference;
import mst.preference.MultiCheckPreference;
import mst.preference.MultiSelectListPreference;
import mst.preference.Preference;
import mst.preference.Preference.OnPreferenceChangeListener;
import mst.preference.Preference.OnPreferenceClickListener;
import mst.preference.PreferenceScreen;
import mst.preference.PreferenceFragment;
import mst.preference.RingtonePreference;
import mst.preference.SeekBarDialogPreference;
import mst.preference.SeekBarPreference;
import mst.preference.SwitchPreference;
import mst.preference.VolumePreference;

import com.mst.demo.MainActivity;
import com.mst.demo.R;
public class PreferenceDemoFragment extends PreferenceFragment  implements OnPreferenceClickListener
,OnPreferenceChangeListener{
	
	private static final String 	TAG = "PreferenceDemoFragment";
	
	private Preference mNormalPrefs;
	
	private PreferenceScreen mPreferenceScreen;
	
	private CheckBoxPreference mCheckBoxPreference;
	
	private SwitchPreference mSwitchPreference;
	
	private SeekBarPreference mSeekBarPreference;
	
	private ListPreference mListPreference;
	
	private EditTextPreference mEditTextPreference;
	
	private SeekBarDialogPreference mSeekBarDialogPreference;
	
	private MultiCheckPreference mMultiCheckPreference;
	
	private MultiSelectListPreference mMultiSelectListPreference;
	
	private RingtonePreference mRingtonePreference;
	
	private VolumePreference mVolumePreference;
	
	private CustomPreference mCustomPreference;
	
	private int mCustomPrefsClickTimes = 0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.demo_preference);
		findPreferences();
		bindListenerToPreference();
	}

	/**
	 * Find all the Preference by key,
	 */
	private void findPreferences(){
		//normal preference
		mNormalPrefs = findPreference("normal_preference");
		
		mPreferenceScreen = (PreferenceScreen)findPreference("preference_screen");
		mPreferenceScreen.setStatus("状态");
		mPreferenceScreen.setStatusIcon(getActivity().getDrawable(R.drawable.icon));
		
		/*
		 * find preference with widget
		 */
		mCheckBoxPreference = (CheckBoxPreference)findPreference("checkbox_preference");
		mSeekBarPreference = (SeekBarPreference)findPreference("seekbar_preference");
		mSwitchPreference = (SwitchPreference)findPreference("switch_preference");
		
		/*
		 * find dialog preference
		 */
		mListPreference = (ListPreference)findPreference("list_preference");
		mEditTextPreference = (EditTextPreference)findPreference("edittext_preference");
		mSeekBarDialogPreference = (SeekBarDialogPreference)findPreference("seek_bar_dialog_preference");
		mMultiCheckPreference = (MultiCheckPreference)findPreference("multi_check_preference");
		mMultiSelectListPreference = (MultiSelectListPreference)findPreference("multi_select_list_preference");
		
		/*
		 * find these preference that can controll sound effects
		 */
		mRingtonePreference = (RingtonePreference)findPreference("ringtong_preference");
		mVolumePreference = (VolumePreference)findPreference("volume_preference");
		
		/*
		 * find custom preference
		 */
		mCustomPreference = (CustomPreference)findPreference("custom_preference_from_code");
	}
	
	
	private void bindListenerToPreference(){
		/*
		 * bind ClickListener
		 */
		mNormalPrefs.setOnPreferenceClickListener(this);
		mSwitchPreference.setOnPreferenceClickListener(this);
		mCustomPreference.setOnPreferenceClickListener(this);
		mPreferenceScreen.setOnPreferenceClickListener(this);
		
		/**
		 * bind ChangeListener
		 */
		mCheckBoxPreference.setOnPreferenceChangeListener(this);
		mListPreference.setOnPreferenceChangeListener(this);
		mEditTextPreference.setOnPreferenceChangeListener(this);
		mMultiCheckPreference.setOnPreferenceChangeListener(this);
		
		/**
		 * bind data changeListener
		 */
		mSeekBarPreference.setOnPreferenceChangeListener(this);
		
	}
	
	@Override
	public boolean onPreferenceChange(Preference preference, Object value) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onPreferenceChange-->"+preference.getKey());
		if(preference == mCheckBoxPreference){
			Log.d(TAG, "onPreferenceChange-->mCheckBoxPreference value--->"+(Boolean)value);
		}else if(preference == mSeekBarPreference){
			Log.d(TAG, "onPreferenceChange-->mSeekBarPreference progress--->"+(Integer)value);
		}else if(preference == mListPreference){
			Log.d(TAG, "onPreferenceChange-->mListPreference newValue--->"+(String)value);
		}else if(preference == mEditTextPreference){
			Log.d(TAG, "onPreferenceChange-->mEditTextPreference text is --->"+(String)value);
		}else if(preference == mMultiCheckPreference){
			boolean[] selectedValues = (boolean[]) value;
			for(boolean v:selectedValues){
				Log.d(TAG, "onPreferenceChange-->mMultiCheckPreference selected --->"+v);
			}
			
		}
		return true;
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onPreferenceClick-->"+preference.getKey());
//		((MainActivity)getActivity()).showActionMode(true);
		if(preference == mCustomPreference){
			mCustomPrefsClickTimes ++;
			mCustomPreference.updateClickedTime(mCustomPrefsClickTimes);
		}
		return true;
	}




}
