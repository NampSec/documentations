package com.mst.demo.material;

import java.util.ArrayList;
import java.util.List;

import com.mst.demo.AgendaFragment;
import com.mst.demo.FragmentAdapter;
import com.mst.demo.InfoDetailsFragment;
import com.mst.demo.ShareFragment;

import mst.widget.DrawerLayout;
import mst.widget.NavigationView;
import mst.widget.ViewPager;
import mst.widget.tab.TabLayout;
import mst.widget.toolbar.Toolbar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mst.demo.R;
public class TabFragment extends Fragment {
	 //Tab菜单，主界面上面的tab切换菜单
    private TabLayout mTabLayout;
    //monster-framework中的ViewPager控件
    private ViewPager mViewPager;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.md_tab, container,false);
		initView(view);
		return view;
	}
	

    private void initView(View view) {
        mTabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) view.findViewById(R.id.view_pager);



        //初始化TabLayout的title数据集
        List<String> titles = new ArrayList<String>();
        titles.add("details");
        titles.add("share");
        titles.add("agenda");
        //初始化TabLayout的title
        mTabLayout.addTab(mTabLayout.newTab().setText(titles.get(0)));
        mTabLayout.addTab(mTabLayout.newTab().setText(titles.get(1)));
        mTabLayout.addTab(mTabLayout.newTab().setText(titles.get(2)));
        //初始化ViewPager的数据集
        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(new TabFrag("Tab1"));
        fragments.add(new TabFrag("Tab2"));
        fragments.add(new TabFrag("Tab3"));
        //创建ViewPager的adapter
        FragmentAdapter adapter = new FragmentAdapter(getFragmentManager(), fragments, titles);
        mViewPager.setAdapter(adapter);
        //千万别忘了，关联TabLayout与ViewPager
        //同时也要覆写PagerAdapter的getPageTitle方法，否则Tab没有title
        mTabLayout.setupWithViewPager(mViewPager);
        
    }
    
    
    class TabFrag extends Fragment{
    	private String name;
    	 public TabFrag(String name) {
			// TODO Auto-generated constructor stub
    		 this.name = name;
		}
    	
    	@Override
    	public View onCreateView(LayoutInflater inflater, ViewGroup container,
    			Bundle savedInstanceState) {
    		// TODO Auto-generated method stub
    		TextView text = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, container,false);
    		text.setText(name);
    		return text;
    	}
    	
    	
    }

}
