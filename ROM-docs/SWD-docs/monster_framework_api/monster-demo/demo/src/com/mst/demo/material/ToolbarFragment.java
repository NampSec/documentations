package com.mst.demo.material;

import mst.app.MstActivity;
import mst.view.menu.bottomnavigation.BottomNavigationView;
import mst.widget.toolbar.Toolbar;
import mst.widget.toolbar.Toolbar.OnMenuItemClickListener;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mst.demo.R;
public class ToolbarFragment extends Fragment  implements OnMenuItemClickListener{
	private static final String 	TAG = "ToolbarFragment";
	private Toolbar mToolbar;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.md_toolbar, container,false);
		mToolbar = (Toolbar)((MstActivity)getActivity()).getToolbar();
		mToolbar.inflateMenu(R.menu.toolbar_menu_demo);
		mToolbar.setOnMenuItemClickListener(this);
		return view;
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onMenuItemClick--->"+item.getTitle());
		return false;
	}
	
	

}
