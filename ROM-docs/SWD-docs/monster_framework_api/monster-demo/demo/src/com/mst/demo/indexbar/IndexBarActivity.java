package com.mst.demo.indexbar;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.Gravity;
import android.widget.AdapterView;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.graphics.Color;
import android.widget.Toast;

import com.mst.demo.R;

import java.util.ArrayList;
import java.util.Objects;

import mst.widget.MstIndexBar;
import mst.widget.MstIndexBar.Letter;

/**
 * 
 * Created by caizhongting on 16-8-5.
 */
public class IndexBarActivity extends Activity implements MstIndexBar.OnSelectListener, AbsListView.OnScrollListener, MstIndexBar.OnTouchStateChangedListener  {
    private MstIndexBar mIndexBar;
    private ListView mListView;
    private ContactAdapter mAdapter;
	private Toast mToast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indexbar);
        mIndexBar = (MstIndexBar) findViewById(R.id.index_bar);
        mIndexBar.setOnSelectListener(this);
        mIndexBar.setOnTouchStateChangedListener(this);
        mListView = (ListView) findViewById(R.id.contact_list);
        mAdapter = new ContactAdapter(this);
        mListView.setAdapter(mAdapter);
        mListView.setOnScrollListener(this);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(IndexBarActivity.this,"click position = "+position,Toast.LENGTH_SHORT).show();
            }
        });
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(IndexBarActivity.this,"long click position = "+position,Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        inflateContacts();
    }


    private void initIndexBar(ArrayList array){
        ArrayList<Letter> sub = null;
        String last = "";
        int lastindex = -1;
        int otherindex = -1;
        boolean changed = false;
        for(int p=0;p<array.size();p++){
            Contact c = (Contact) array.get(p);
            int namesize = c.firstLetter.size();
            String firletter = "",secletter = "";
            for(int i=0;i<namesize;i++) {
                if(i == 0) {
                    firletter = c.firstLetter.get(0);
                    changed = !firletter.equals(last);
                    last = firletter;
                }else if(i == 1){
                    secletter = c.firstLetter.get(1);
                }
            }
            if(changed){
                if(sub != null && lastindex != -1){
                    mIndexBar.setSubList(lastindex,sub);
                }
                if(!"".equals(firletter)) {
                    int index = mIndexBar.getIndex(firletter);
                    if(index != -1) {
                        lastindex = index;
                        sub = new ArrayList<>();
                    }else{
                        sub = null;
                    }
                    if(index == -1){//其他（#）的索引
                        index = mIndexBar.size()-1;
                        if(otherindex == -1){
                            otherindex = p;
                        }
                    }
                    //设置第一个字母对应的列表索引
                    Letter letter = mIndexBar.getLetter(index);
                    if(letter != null){
                        letter.list_index = index == mIndexBar.size()-1 ? otherindex : p;
                    }
                    mIndexBar.setEnables(true,index);
                }
            }
            //设置第二个字母的列表索引
            if(sub != null && secletter != "") {
                if(!sub.contains(Letter.valueOf(secletter))) {
                    Letter letter = Letter.valueOf(secletter);
                    letter.enable = true;
                    letter.list_index = p;
                    sub.add(letter);
                }
            }
        }
        if(sub != null && lastindex != -1){
            mIndexBar.setSubList(lastindex,sub);
        }

    }

    private void inflateContacts(){
        new AsyncTask<Object,Object,ArrayList>(){
            @Override
            protected ArrayList doInBackground(Object... params) {
                return Been.getContacts();
            }

            @Override
            protected void onPostExecute(ArrayList arrayList) {
                mAdapter.setData(arrayList);
                mAdapter.notifyDataSetChanged();
                initIndexBar(arrayList);
            }
        }.execute();
    }

    @Override
    public void onSelect(int index, int layer, MstIndexBar.Letter letter) {
        int listindex = letter.list_index;
        if(layer == 0){
            listindex--;
        }
        mListView.setSelection(listindex);
    }

    @Override
    public void onStateChanged(MstIndexBar.TouchState old, MstIndexBar.TouchState news) {
		if(mToast != null){
			mToast.cancel();
		}
        mToast = Toast.makeText(this,"Touch state : "+news,Toast.LENGTH_SHORT);
		mToast.show();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Contact contact = (Contact) mAdapter.getItem(firstVisibleItem);
        if(contact != null) {
            int index = -1;
            if (contact.firstLetter != null && contact.firstLetter.size() > 0) {
                String fir = contact.firstLetter.get(0);
                index = mIndexBar.getIndex(fir);
            } else {
                String fir = contact.name;
                index = mIndexBar.getIndex(fir);
            }

            if(index == -1){
                index = mIndexBar.size() - 1;
            }

            mIndexBar.setFocus(index);
        }
    }
}
