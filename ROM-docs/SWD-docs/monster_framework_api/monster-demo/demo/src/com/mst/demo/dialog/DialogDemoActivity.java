package com.mst.demo.dialog;

import mst.app.MstActivity;
import mst.widget.toolbar.Toolbar;
import android.app.Activity;
import android.os.Bundle;

import com.mst.demo.R;
public class DialogDemoActivity extends MstActivity{
	private Toolbar mToolbar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setMstContentView(R.layout.main_activity);
		mToolbar = getToolbar();
		mToolbar.setTitle("Dialog Demo");
		getFragmentManager().beginTransaction()
        .replace(R.id.content, new DialogDemoFragment())
        .commit();

	}
	
	
}
