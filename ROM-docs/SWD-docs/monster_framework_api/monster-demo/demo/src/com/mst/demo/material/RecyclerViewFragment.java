package com.mst.demo.material;

import mst.widget.DrawerLayout;
import mst.widget.DrawerLayout.DrawerListener;
import mst.widget.NavigationView;
import mst.widget.NavigationView.OnNavigationItemSelectedListener;
import mst.widget.recycleview.GridLayoutManager;
import mst.widget.recycleview.LinearLayoutManager;
import mst.widget.recycleview.RecyclerView;
import mst.widget.recycleview.RecyclerViewDivider;
import mst.widget.recycleview.RecyclerViewGridItemSpace;
import mst.widget.toolbar.Toolbar;
import mst.widget.toolbar.Toolbar.OnMenuItemClickListener;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.mst.demo.R;
import com.mst.demo.RecyclerViewAdapter;

public class RecyclerViewFragment extends Fragment implements OnClickListener {
	private RecyclerView mRecyclerView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.md_recyclerview_layout,
				container, false);
		mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
		View horizentalView = view.findViewById(R.id.linear_horizental_btn);
		View verticalView = view.findViewById(R.id.linear_vertical_btn);
		View gridView = view.findViewById(R.id.linear_grid_btn);
		horizentalView.setOnClickListener(this);
		verticalView.setOnClickListener(this);
		gridView.setOnClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		/*
		 * 设置RecyclerView线性水平布局
		 */
		case R.id.linear_horizental_btn:
			mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
			mRecyclerView.addItemDecoration(new RecyclerViewDivider(getActivity(), LinearLayoutManager.HORIZONTAL));
			break;
			/*
			 * 设置RecyclerView线性垂直布局
			 */
		case R.id.linear_vertical_btn:
			mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
			mRecyclerView.addItemDecoration(new RecyclerViewDivider(getActivity(), LinearLayoutManager.VERTICAL));
			break;
			/*
			 * 设置RecyclerView网格布局
			 */
		case R.id.linear_grid_btn:
			mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
			mRecyclerView.addItemDecoration(new RecyclerViewGridItemSpace(10, 3));
			break;

		default:
			break;
		}
		mRecyclerView.setAdapter(new RecyclerViewAdapter(getActivity()));
	}

}
