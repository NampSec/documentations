package com.mst.demo.material;

import mst.app.MstActivity;

import com.mst.demo.preference.PreferenceDemoFragment;
import com.mst.demo.util.Actions;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;

import com.mst.demo.R;
public class MaterialDemoActivity extends MstActivity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setMstContentView(R.layout.main_activity);
		Intent intent = getIntent();
		int position = intent.getIntExtra(Actions.MATERIAL_WIDGET_ITEMS_POSITION, 0);
		String name = intent.getStringExtra(Actions.MATERIAL_WIDGET_ITEMS_NAME);
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Actions.switchToFragment(this,position);
		getToolbar().setTitle(name);
		
	}
	
	

}
